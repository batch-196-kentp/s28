// db.users.insertOne({
// 
//     "username": "lebronGOATtheGoat",
//     "password": "hatersGonnaHateHate"
// 
//     
// 
// })
// 
// db.users.insertOne({
// 
//     "username": "gokuSon",
//     "password": "over9000"
// 
// 
// }) 
// 
// 


//insert multiple documents at once
// db.users.insertMany(
// 
//     [
//         {
//            "username": "pablo123",
//             "password": "123paul"
//         },
//         {
//            "username": "pedro99",
//             "password": "iampeter99"
//             
//         }
//     
//     
//     ]
// 
// )
// 
// db.products.insertMany ([
//     {
//         "name": "filter60",
//         "description": "air filter",
//         "price": 1500
//     },
//     {
//         "name": "belt12h",
//         "description": "fan belt",
//         "price": 3000
//     },
//     {
//         "name": "engine_oil",
//         "description": "lubri 90",
//         "price": 1200
//     },
// 
// ])
// 
    
//Read/Retrieve
    
//db.collection.find() - return/find all documents in the collenction
//db.users.find()

// db.users.find({"username": "pedro99"})
// 
// 
// db.cars.insertMany ([
// 
//     {
//         "name": "Vios",
//         "brand": "Toyota",
//         "type": "sedan",
//         "price": 1500000
//     },
//     {
//         "name": "Tamaraw FX",
//         "brand": "Toyota",
//         "type": "auv",
//         "price":750000
//     },
//     {
//         "name": "City",
//         "brand": "Honda",
//         "type": "sedan",
//         "price": 1600000
//     },
// ])
    
    
// db.cars.find({"type": "sedan"})
//db.cars.find({"brand": "Toyota"})

//db.cars.findOne({})
// find/return the first item/document in the collection

//db.cars.findOne({"type": "sedan"})

//db.cars.findOne({"brand": "Toyota"})
//db.cars.findOne({"brand": "Honda"})


//UPDATE
//db.collection.updateOne ({"criteria":"value"},{$set:{"fieldToBeUpdated":"Updated Value"}})

//db.users.updateOne({"username": "pedro99"},{$set:{"username": "peter1999"}})

//db.users.updateOne({},{$set:{"username": "updatedUsername"}})


//if the field does not exist, mongodb will instead
//add that field into the document
//db.users.updateOne({"username": "pablo123"},{$set:{"isAdmin":true}})


//db.collecntion.updateMany({},{$set:{}})
//db.users.updateMany({},{$set:{"isAdmin":true}})
//db.cars.updateMany({"type":"sedan"},{$set:{"price":1000000}})

//DELETE
//db.collection.deleteOne({}) - delete first item in collection
//db.products.deleteOne({})

//db.cars.deleteOne({"brand":"Toyota"})
//db.users.deleteMany({"isAdmin":true}) --deletes all items that matchesthe criteria

//db.collection.deleteMany({}) --delete all documents in the collection

//db.products.deleteMany({})
db.cars.deleteMany({})






